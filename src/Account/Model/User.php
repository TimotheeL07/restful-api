<?php

namespace Src\Account\Model;

use \Illuminate\Database\Query\Builder;

class User extends \Illuminate\Database\Eloquent\Model {

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password',
    ];

    public $timestamps = false;

    public static function get_id($user) {
        $user = Users::where('username', '=', $user)->first();
        return $user->id;
    }

    public static function create($user) {
        
        $id = User::insertGetId([
                    'name' => $user['name'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'password' => password_hash($user['password'], PASSWORD_DEFAULT),
                    'statut' => 'valid',
                    'date_add' => date('Y-m-d H:i:s'),
                    'date_upd' => date('Y-m-d H:i:s'),
        ]);
            
        return $id;
    }

    public static function myUpdate($user,$id) {
        var_dump($user);die();
        $user = User::findOrFail($id)->update([
                    'name' => $user['name'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'password' => password_hash($user['password'], PASSWORD_DEFAULT),
                    'statut' => 'valid',
                    'date_upd' => date('Y-m-d H:i:s')
        ]);
            
        return $user->id;
    }
}
