<?php

namespace Src\Account\Controller;

use \Illuminate\Database\Query\Builder;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface as Container;

use Src\Account\Model\User;

class UserController {


    public function __construct(Container $container) {
       $this->container = $container;
    }

    public function home(Request $request, Response $response)
    {
         return $response->write("Welcome to the UserController");
    }

    public function getUser(Request $request, Response $response, $args)
    {
        // $Users = $this->container->get('db')->table('users');

        $user = User::find(intval($args['id']));

        if($user){
          return $response->withJson($user);
        }
        
        return $response->withJson(['data' => 'User not found'], 404);
    }

    public function updateUser(Request $request, Response $response, $args)
    {
        // var_dump( $request->getParams() );die();
        $user = User::myUpdate($request->getParams(), $args['id']);

        if($user){
          return $response->withJson($user);
        }
        
        return $response->withJson(['data' => 'User not found'], 404);
    }
}
