<?php

namespace Src\Account\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface as Container;

class AccountController {

    // constructor receives container instance
    public function __construct(Container $container) {
       $this->container = $container;
    }

    public function login(Request $request, Response $response)
    {

    }

    public function signup(Request $request, Response $response)
    {
        
    }

    public function newPassword(Request $request, Response $response)
    {
        
    }

}