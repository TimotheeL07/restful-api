<?php

namespace Src\Book\Controller;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Container\ContainerInterface as Container;

class BookController {

    // constructor receives container instance
   public function __construct(Container $container) {
       $this->container = $container;
   }

    public function home(Request $request, Response $response)
    {
        // $my_logger = $this->container->get('logger');
        // $my_logger->addInfo("Something interesting happened");

        $my_cache = $this->container->get('cache');

        $res = $response->write("Welcome to the IndexController::Home");

        // Use the HttpCache
        $resWithEtag = $my_cache->withEtag($res, '');

        return $resWithEtag;
    }

    public function hello(Request $request, Response $response)
    {
        $name = $request->getAttribute('name');
        return $response->write("Hello, $name");    
    }
}