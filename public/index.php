<?php


require __DIR__."../../vendor/autoload.php";

$dotenv = new Dotenv\Dotenv(__DIR__.'/..');
$dotenv->load();

$settings = require __DIR__ . '/../var/config/config.php';

$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../app/dependencies.php';

require __DIR__."../../app/routes.php";

$app->run();