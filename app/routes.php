<?php

$app->get('/', \Src\Book\Controller\BookController::class . ':home');

$app->get('/hello/{name}', \Src\Book\Controller\BookController::class . ':hello');

$app->get('/account/profile/{id}', \Src\Account\Controller\UserController::class . ':getUser');

$app->put('/user/update/{id}', \Src\Account\Controller\UserController::class . ':updateUser');

