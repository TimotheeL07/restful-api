<?php

// DIC configuration
$container = $app->getContainer();

// cache renderer
$container['cache'] = function ($c) {
    return new \Slim\HttpCache\CacheProvider();
};

// Use eloquent
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection([
            'driver' => 'mysql',
            'host' => getenv("DB_HOST"),
            'port' => getenv("DB_PORT"),
            'database' =>  getenv("DB_NAME"),
            'username' => getenv("DB_USER"),
            'password' => getenv("DB_PASSWORD"),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => ''
    ]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

// $container['db'] = function ($c) {
//     return $capsule;
// };

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write('Page not found');
    };
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};